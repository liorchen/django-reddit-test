# Django-Reddit

This is a simple project that integrates django, django-rest-framework and reddit api.

It Allows you to:
  - Register a user
  - Login
  - See your favorite subreddits
  - get the number of times a certain text repeats in a subreddit

## How to run it
  - run ```docker-compose up```
  - open browser
  - go to ```http://localhost:8080```
  - click on the create account link
  - you'll see a drf page which contains a form in the bottom, fill the form with some user info.
  - click on the 'login' button on the top-right area of the page
  - do the login with the user you just created
  - go again to ```http://localhost:8080```
  - you should see a link that leads you to reddit authentication page, click on it!.
  - approve the app
  - you should then get redirected to ```/api/reddit/my-subreddits``` where you can see your favorite subreddits
  - to see how much a certain word repeats in a subrredit, go to ```/api/reddit/word-count?q=[SEARCH_TEXT]``` (if you provide ```subreddit_id``` in the query string, it will search in the provided reddit, otherwise it will search in the first subrredit in your favorites)

## Some notes

- There's no fancy UI as you can see, only django/drf default UI (consider this project more as an API rather then a website)
- This is just for local testing, so the server is based on django's ```runserver``` command and the db is sqlite.


### Development

Want to contribute? Great!

# CHEERS!