
from django.db import models
from django.conf import settings

# class UserReditCredentials(models.Model):
#     user = models.ForeignKey('User', unique=True)
#     client_secret = models.CharField(max_length=100)
#     client_id = models.CharField(max_length=50)
#     created_at = models.DateTimeField(auto_now_add=True, blank=True)

class RedditAuthState(models.Model):
    state = models.CharField(max_length=100, db_index=True)
    access_token = models.CharField(max_length=100, null=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    user = models.OneToOneField(to=settings.AUTH_USER_MODEL, null=False)