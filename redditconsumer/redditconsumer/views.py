import urllib

from django.db import transaction, IntegrityError
from django.http import HttpResponse
from django.template import loader
from rest_framework.exceptions import PermissionDenied

from redditclientlib import Client, API, UnAuthException, RateLimitException
from redditcreds.models import RedditAuthState
from django.shortcuts import render, get_object_or_404, redirect
import json
redit_client = Client("BpWe3EdYBC1TDA", '9Vv3ZkNEbYSCCNlQ42xvORi3zFw', "http://localhost:8080/auth-callback")


def reddit_callback(request):
    current_state = get_object_or_404(RedditAuthState, user=request.user, state=request.GET.get('state'))
    try:
        api = redit_client.get_api(request.GET.get('code'))
        me = api.me()
        current_state.access_token = api.access_token
        current_state.save()
        return redirect("my-subreddits")
        # return HttpResponse(json.dumps(me))
    except RateLimitException:
        raise PermissionDenied("too many reddit requests, please try again later", 429)
    except UnAuthException:
        raise PermissionDenied("reddit user failed to authenticate")


@transaction.atomic()
def index(request):
    if not request.user.is_authenticated():
        return redirect('start')
    try:
        current_user_token = RedditAuthState.objects.get(user=request.user, access_token__isnull=False)

        api = API(current_user_token.access_token)

        try:
            me = api.me()
            return redirect("my-subreddits")
        except (UnAuthException, RateLimitException):
            # token expired
            RedditAuthState.objects.filter(user=request.user).delete()
            return redirect('start')

    except RedditAuthState.DoesNotExist:
        RedditAuthState.objects.filter(user=request.user).delete()
        url, state = redit_client.make_authorization_url()
        RedditAuthState(state=state, user=request.user).save()

        context = {
            'redit_auth_uri': url,
        }
        return render(request, 'redditconsumer/start-reddit.html', context)


def start(request):
    if request.user.is_authenticated:
        return redirect('index')
    return render(request, 'redditconsumer/login-or-register.html')
