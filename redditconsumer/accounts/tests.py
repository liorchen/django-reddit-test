
from django.contrib.auth.models import User
from django.test import TestCase



class AccountModelTests(TestCase):

    def test_create_user_success(self):
        """
        checks that user is actually created in the db
        """
        response = self.client.post('/api/account/',
                                    data={"username": "test1", "password":  "Aa123456", "first": "lior", "last": "chen"})

        user_count = User.objects.filter(username="test1").count()
        self.assertEqual(user_count, 1)
        self.assertEqual(response.status_code, 201)

