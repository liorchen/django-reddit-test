from django.conf.urls import url, include
from rest_framework import routers

from accounts.views import UserViewSet, CreateAccount
from . import views

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'account', CreateAccount, base_name='account-create')

urlpatterns = [
    url(r'', include(router.urls)),
]