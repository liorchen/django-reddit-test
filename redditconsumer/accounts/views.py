# ViewSets define the view behavior.
from django.contrib.auth.models import User
from rest_framework import viewsets, mixins
from rest_framework.viewsets import GenericViewSet

from accounts.serializers import UserSerializer

from rest_framework.authtoken.models import Token
from django.db import transaction

class CreateAccount(mixins.CreateModelMixin, GenericViewSet):
    """
    Creates the user.
    """
    serializer_class = UserSerializer
    authentication_classes = ()
    permission_classes = ()


    @transaction.atomic()
    def perform_create(self, serializer):
        user = serializer.save()
        if user:
            Token.objects.create(user=user)


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer