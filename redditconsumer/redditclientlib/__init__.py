from uuid import uuid4
from urllib.parse import urlencode
from requests import auth as requests_auth
import requests
import logging
base_header = { "User-Agent": "gandon.mymo-challenge.v1" }
# Get an instance of a logger
logger = logging.getLogger(__name__)


class UnAuthException(Exception):
    pass

class RateLimitException(Exception):
    pass

class API(object):
    base_url = "https://oauth.reddit.com"

    def __init__(self, access_token):
        self.access_token = access_token
        self.headers = { "Authorization": "bearer " + access_token }
        self.headers.update(base_header)

    def validate_resp(self, resp):
        if resp.status_code == 429:
            raise RateLimitException()
        if resp.status_code >= 400:
            logger.warning("got error from reddit %s" % resp)
            raise UnAuthException()
        else:
            return resp

    def me(self):
        response = requests.get(self.base_url + "/api/v1/me", headers=self.headers)
        self.validate_resp(response)
        return response.json()

    def scopes(self):
        response = requests.get(self.base_url + "/api/v1/scopes", headers=self.headers)
        self.validate_resp(response)
        return response.json()

    def my_subrredits(self):
        response = requests.get(self.base_url + "/subreddits/mine", headers=self.headers)
        self.validate_resp(response)
        return response.json()

    def search(self, subid, q, after):
        response = requests.get("https://www.reddit.com/r/%s/search.json?q=%s&limit=100&after=%s" % (subid, q, after), headers=base_header)
        self.validate_resp(response)
        return response.json()



class Client(object):
    def __init__(self, client_id,
                 client_secret,
                 redirect_uri,
                 scopes="identity mysubreddits read"):
        self._client_id = client_id
        self._client_secret = client_secret
        self._redirect_uri = redirect_uri
        self._scopes = scopes

    def make_authorization_url(self):
        # Generate a random string for the state parameter
        # Save it for use later to prevent xsrf attacks
        state = str(uuid4())
        params = {"client_id": self._client_id,
                  "response_type": "code",
                  "state": state,
                  "redirect_uri": self._redirect_uri,
                  "duration": "temporary",
                  "scope": self._scopes
        }

        url = "https://ssl.reddit.com/api/v1/authorize?" + urlencode(params)
        return url, state


    def get_api(self, code):
        client_auth = requests_auth.HTTPBasicAuth(self._client_id, self._client_secret)
        post_data = {"grant_type": "authorization_code",
                     "code": code,
                     "redirect_uri": self._redirect_uri}
        response = requests.post("https://ssl.reddit.com/api/v1/access_token",
                                 auth=client_auth,
                                 data=post_data,
                                 headers=base_header)
        try:
            response.raise_for_status()
        except requests.HTTPError as e:
            logger.warning("got response from reddit: %s", e)
            raise RateLimitException() if response.status_code == 429 else UnAuthException()
        token_json = response.json()
        return API(token_json["access_token"])

