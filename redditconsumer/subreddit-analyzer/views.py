import re
from rest_framework import viewsets, mixins
from rest_framework.exceptions import PermissionDenied, ValidationError
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet
from redditcreds.models import RedditAuthState
from redditclientlib import API, UnAuthException, RateLimitException
from django.shortcuts import get_object_or_404
import time


class SubredditAPIMixin(object):
    def get(self, request, format='json'):
        state = get_object_or_404(RedditAuthState, user=request.user, access_token__isnull=False)
        api = API(state.access_token)
        try:
            return Response(data=self.with_api(api, request))
        except RateLimitException:
            raise PermissionDenied(
                "reddit has unauthorized the request due to ratelimit. please try again later", 429)
        except UnAuthException:
            raise PermissionDenied("reddit has unauthorized the request due to permissions/authentication issue")

    def with_api(self, api, request):
        pass

class GetSubreddits(SubredditAPIMixin, APIView):
    def with_api(self, api, request):
        return api.my_subrredits()

class WordCountInSubreddit(SubredditAPIMixin, APIView):

    def _get_count_in_post(self, post, search_text):
        return len(re.findall(search_text, post['title'], flags=re.IGNORECASE)) + \
            len(re.findall(search_text, post.get('selftext', ''), flags=re.IGNORECASE))


    def with_api(self, api, request):
        """
        after getting the API object for the current user, do something with it in order to return with the response
        :param api:
        :param request:
        :return: some native type to be serialized to json
        """
        try:
            subid = request.GET.get('subreddit_id', None)
            if not subid:
                subid = api.my_subrredits()['data']['children'][0]['data']["display_name"]
            try:
                search_text = request.GET['q']
            except KeyError:
                raise ValidationError("q is required")
            skip = 0
            response = api.search(subid, search_text, '')['data']
            data, after = response['children'], response['after']
            counter = 0
            start = time.time()
            while len(data) > 0 and (time.time() - start) < 30: # finish after 30s
                counter += sum(self._get_count_in_post(x['data'], search_text) for x in data)
                skip += len(data)
                response = api.search(subid, search_text, after)['data']
                data, after = response['children'], response['after']

            return {"subreddit": subid, "iterated": skip, "occurrences": counter}

        except IndexError:
            return Response(data="no subreddit found", status=404)

