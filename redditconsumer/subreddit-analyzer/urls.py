from django.conf.urls import url
from .views import GetSubreddits, WordCountInSubreddit

urlpatterns = [
    url(r'reddit/my-subreddits', GetSubreddits.as_view(), name="my-subreddits"),
    url(r'reddit/word-count', WordCountInSubreddit.as_view(), name="word-count"),

]