FROM python:3.6.3-alpine3.6

ADD redditconsumer /app
WORKDIR /app
RUN pip install -r requirements.txt
RUN python manage.py test -v 3
RUN rm -f db.sqlite3
RUN python manage.py migrate

CMD ["python", "manage.py", "runserver", "0.0.0.0:8080"]
